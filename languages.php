<?php

$CDU_LANGUAGES = array();

$CDU_LANGUAGES["EN"] = [
    "CACHE_EQUAL_REMOTE_MESSAGE" => "OK    - My IP is equal than local cache",
    "REAL_EQUAL_DOMAIN_MESSAGE" => "OK    - My IP is equal than cPanel DNS",
    "RETRIVE_ERROR_MESSAGE" => "ERROR - Cannot read my IP remotely",
    "UNKNOW_UPDATE_ERROR_MESSAGE" => "ERROR - unknown error while updating IP",
    "DNS_IP_UPDATED_MESSAGE" => "OK    - IP updated to: ",
    "CANNOT_CONNECT_CPANEL" => "ERROR - Cannot connect to cpanel or API error",
];

$CDU_LANGUAGES["PT_BR"] = [
    "CACHE_EQUAL_REMOTE_MESSAGE" => "OK   - Meu ip é igual ao cache local",
    "REAL_EQUAL_DOMAIN_MESSAGE" => "OK   - Meu IP é igual ao DNS no cPanel",
    "RETRIVE_ERROR_MESSAGE" => "ERRO - Não foi possível ler meu ip remotamente",
    "UNKNOW_UPDATE_ERROR_MESSAGE" => "ERRO - erro desconhecido - não foi possivel atualizar meu IP",
    "DNS_IP_UPDATED_MESSAGE" => "OK   - IP atualizado para: ",
    "CANNOT_CONNECT_CPANEL" => "ERRO - Não foi possível conectar ao Cpanel ou erro de API",
];